package in.wenwen.test;

public class TestArray {
    public static void main(String[] args) {
        String[] strs = new String[3];
        Class<?> clazz = strs.getClass();
        System.out.println(clazz.getComponentType());
        
        float f = 88.22f;
        System.out.println(f);
        
        System.out.println("long max = " + Long.MAX_VALUE);
        System.out.println("long max = " + (Long.MAX_VALUE + 1));
    }
}

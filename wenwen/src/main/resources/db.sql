
-- 创建库
-- CREATE DATABASE `wenwen` CHARSET utf8 COLLATE utf8_general_ci;

DROP TABLE IF EXISTS `sso`;
CREATE TABLE `sso`(
    `id` BIGINT(16) NOT NULL AUTO_INCREMENT,
    `user_id` BIGINT(16) NOT NULL,
    `token` VARCHAR(32) NOT NULL,
    `create_datetime` DATETIME,
    `modify_datetime` DATETIME,
    PRIMARY KEY(`id`),
    KEY `index_0`(`token`)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=UTF8;

DROP TABLE IF EXISTS `webapp`;
CREATE TABLE `webapp` (
  `id` bigint(16) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL,
  `descript` TEXT,
  `is_halt` varchar(2) DEFAULT 'F',
  `create_datetime` datetime DEFAULT NULL,
  `modify_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
INSERT INTO `webapp`(`id`, `name`, `descript`) VALUE(1, '主站', '一个神奇的博客');

DROP TABLE IF EXISTS `domain`;
CREATE TABLE `domain` (
  `id` BIGINT(16) NOT NULL AUTO_INCREMENT,
  `webapp_id` BIGINT(16) DEFAULT NULL,
  `site` VARCHAR(128) NOT NULL,
  `main` VARCHAR(256) DEFAULT '/index' COMMENT '默认主页',
  `templet` VARCHAR(64) NOT NULL DEFAULT 'default',
  `is_halt` VARCHAR(2) DEFAULT 'F',
  `create_datetime` DATETIME DEFAULT NULL,
  `modify_datetime` DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_0` (`site`)
)ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
INSERT INTO `domain`(`webapp_id`,`site`,`templet`) VALUE(1,'localhost','underriver');
INSERT INTO `domain`(`webapp_id`,`site`,`templet`) VALUE(1,'127.0.0.1','underriver');

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(16) NOT NULL AUTO_INCREMENT,
  `webapp_id` bigint(16) NOT NULL,
  `uid` varchar(64) NOT NULL,
  `nick` varchar(128) DEFAULT '',
  `password` varchar(32) NOT NULL,
  `create_ip` varchar(16) DEFAULT NULL,
  `modify_ip` varchar(16) DEFAULT NULL,
  `create_datetime` datetime DEFAULT NULL,
  `modify_datetime` datetime DEFAULT NULL,
  `is_halt` varchar(2) DEFAULT 'F',
  `email` varchar(128) DEFAULT NULL,
  `mobile` varchar(32) DEFAULT NULL,
  `sex` varchar(2) DEFAULT 'G',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_0` (`webapp_id`,`uid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

INSERT INTO `user`(`id`, `webapp_id`, `uid`, `nick`, `password`, `create_ip`, `modify_ip`, `create_datetime`, `modify_datetime`, `is_halt`, `email`, `mobile`, `sex`) VALUE('1','1','admin','admin',MD5('admin'),'127.0.0.1','127.0.0.1',NOW(),NOW(),'F','916673309@qq.com',NULL,'G');

DROP TABLE IF EXISTS `user_auth`;
CREATE TABLE `user_auth`(
    `id` BIGINT(16) NOT NULL AUTO_INCREMENT COMMENT '主键',
    `webapp_id` BIGINT(16) NOT NULL,
    `uri` VARCHAR(128) NOT NULL COMMENT 'uri',
    `descript` VARCHAR(56),
    `descript_en` VARCHAR(56),
    `is_halt` VARCHAR(2) NOT NULL DEFAULT 'F' COMMENT '是否停用',
    `create_datetime` DATETIME,
    `modify_datetime` DATETIME,
    PRIMARY KEY (`id`),
    UNIQUE KEY `index_0`(`webapp_id`,`uri`)
);

DROP TABLE IF EXISTS `user_auth_cache`;
CREATE TABLE `user_auth_cache`(
    `id` BIGINT(16) NOT NULL AUTO_INCREMENT COMMENT '主键',
    `webapp_id` BIGINT(16) NOT NULL,
    `user_id` BIGINT(16),
    `cache_string` VARCHAR(256),
    `is_halt` VARCHAR(2) NOT NULL DEFAULT 'F' COMMENT '是否停用',
    `create_datetime` DATETIME,
    `modify_datetime` DATETIME,
    PRIMARY KEY (`id`),
    KEY `index_0`(`webapp_id`,`user_id`)
);

DROP TABLE IF EXISTS `upload_file`;
CREATE TABLE `upload_file` (
  `id` BIGINT(16) NOT NULL AUTO_INCREMENT,
  `file_type` VARCHAR(20) NOT NULL DEFAULT '',
  `dir_code` VARCHAR(2) NOT NULL DEFAULT 'A',
  `dir` VARCHAR(16) NOT NULL,
  `name` VARCHAR(32) NOT NULL,
  `size` BIGINT(16) NOT NULL,
  `real_name` VARCHAR(255) NOT NULL,
  `remark` VARCHAR(512) DEFAULT NULL,
  `create_datetime` DATETIME NOT NULL,
  `modify_datetime` DATETIME NOT NULL,
  PRIMARY KEY(`id`),
  KEY index_0(`name`)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

INSERT INTO `upload_file` (`id`, `file_type`, `dir_code`, `dir`, `name`, `size`, `real_name`, `remark`, `create_datetime`, `modify_datetime`) VALUES('1','png','A','2017/03/04','88feebed69ab10431a13d3864c06cfc9','113124','88feebed69ab10431a13d3864c06cfc9.png',NULL,'2017-03-04 09:52:10','2017-03-04 09:52:10');
INSERT INTO `upload_file` (`id`, `file_type`, `dir_code`, `dir`, `name`, `size`, `real_name`, `remark`, `create_datetime`, `modify_datetime`) VALUES('2','png','A','2017/03/04','53fc76601b4e1b6f0616ad6692212056','359412','hello.png',NULL,'2017-03-04 10:03:03','2017-03-04 10:03:03');

DROP TABLE IF EXISTS `article`;
CREATE TABLE `article`(
    `id` BIGINT(16) NOT NULL AUTO_INCREMENT,
    `webapp_id` BIGINT(16) NOT NULL,
    `type_id` BIGINT(16) NOT NULL,
    `user_id` BIGINT(16) NOT NULL,
    `title` VARCHAR(512),
    `content` TEXT,
    `tags` VARCHAR(256) DEFAULT '' COMMENT ',分割',
    `click` BIGINT(16) DEFAULT 0,
    `assess` BIGINT(16) DEFAULT 0,
    `is_save` VARCHAR(2) DEFAULT 'F',
    `is_halt` VARCHAR(2) DEFAULT 'F',
    `create_datetime` DATETIME,
    `modify_datetime` DATETIME,
    PRIMARY KEY (`id`)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

INSERT INTO `article` (`id`, `webapp_id`, `type_id`, `user_id`, `title`, `content`, `tags`, `click`, `assess`, `is_save`, `is_halt`, `create_datetime`, `modify_datetime`) VALUES('1','1','1','1','根与叶的守望','<div style=\"text-align:center;\">\r\n  <img src=\"/upload/2017/03/04/88feebed69ab10431a13d3864c06cfc9\" alt=\"\" />\r\n</div>','守望','5','1','F','F','2017-03-04 09:53:13','2017-03-04 09:57:36');
INSERT INTO `article` (`id`, `webapp_id`, `type_id`, `user_id`, `title`, `content`, `tags`, `click`, `assess`, `is_save`, `is_halt`, `create_datetime`, `modify_datetime`) VALUES('2','1','1','1','本站博客永久开源','<p style=\"text-align:center;\">\r\n  <img src=\"/upload/2017/03/04/53fc76601b4e1b6f0616ad6692212056\" alt=\"\" width=\"768\" height=\"1285\" title=\"\" align=\"\" />\r\n</p>\r\n<p style=\"text-align:left;\">\r\n  <p style=\"font-family:&quot;font-size:16px;vertical-align:baseline;color:rgba(0, 0, 0, 0.8);\">\r\n        应广大朋友建议，本站博客永久开源。\r\n   </p>\r\n    <p style=\"font-family:&quot;font-size:16px;vertical-align:baseline;color:rgba(0, 0, 0, 0.8);\">\r\n        开源地址：http://git.oschina.net/jiucheng_org/wenwen\r\n </p>\r\n    <p style=\"font-family:&quot;font-size:16px;vertical-align:baseline;color:rgba(0, 0, 0, 0.8);\">\r\n        轻量级jiucheng框架开源地址：http://git.oschina.net/jiucheng_org/jiucheng\r\n  </p>\r\n    <p style=\"font-family:&quot;font-size:16px;vertical-align:baseline;color:rgba(0, 0, 0, 0.8);\">\r\n        博客使用了jiucheng框架，由于框架没有提交到中央仓库，所以想本地调试运行本站博客源码，需要下载jiucheng框架。\r\n   </p>\r\n    <p style=\"font-family:&quot;font-size:16px;vertical-align:baseline;color:rgba(0, 0, 0, 0.8);\">\r\n        如碰到无法安装可以留言或者联系本人。<span style=\"font-family:&quot;\"></span>\r\n    </p>\r\n</p>','博客,轻量级','4','1','F','F','2017-03-04 10:04:01','2017-03-04 10:06:42');

DROP TABLE IF EXISTS `article_type`;
CREATE TABLE `article_type`(
    `id` BIGINT(16) NOT NULL AUTO_INCREMENT,
    `webapp_id` BIGINT(16) NOT NULL,
    `name` VARCHAR(64) NOT NULL,
    `size` BIGINT(16),
    `is_halt` VARCHAR(2) DEFAULT 'F',
    `create_datetime` DATETIME,
    `modify_datetime` DATETIME,
    PRIMARY KEY (`id`),
    UNIQUE KEY `index_0`(`webapp_id`,`name`)
);

INSERT INTO `article_type` (`id`, `webapp_id`, `name`, `size`, `is_halt`, `create_datetime`, `modify_datetime`) VALUES('1','1','一人一物','2','F','2017-03-04 09:49:22','2017-03-04 10:04:01');

DROP TABLE IF EXISTS `tag`;
CREATE TABLE `tag` (
  `id` bigint(16) NOT NULL AUTO_INCREMENT,
  `webapp_id` BIGINT(16) NOT NULL,
  `name` varchar(32) NOT NULL,
  `size` BIGINT(16) DEFAULT 0,
  `is_halt` varchar(2) DEFAULT 'F',
  `create_datetime` datetime DEFAULT NULL,
  `modify_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_0`(`webapp_id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tag` (`id`, `webapp_id`, `name`, `size`, `is_halt`, `create_datetime`, `modify_datetime`) VALUES('1','1','守望','1','F',NULL,NULL);
INSERT INTO `tag` (`id`, `webapp_id`, `name`, `size`, `is_halt`, `create_datetime`, `modify_datetime`) VALUES('2','1','博客','1','F',NULL,NULL);
INSERT INTO `tag` (`id`, `webapp_id`, `name`, `size`, `is_halt`, `create_datetime`, `modify_datetime`) VALUES('3','1','轻量级','1','F',NULL,NULL);

DROP TABLE IF EXISTS `article_tag`;
CREATE TABLE `article_tag` (
  `id` bigint(16) NOT NULL AUTO_INCREMENT,
  `webapp_id` BIGINT(16) NOT NULL,
  `article_id` BIGINT(16) NOT NULL,
  `tag_id` BIGINT(16) NOT NULL,
  `is_halt` varchar(2) DEFAULT 'F',
  `create_datetime` datetime DEFAULT NULL,
  `modify_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_0`(`webapp_id`,`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `article_tag` (`id`, `webapp_id`, `article_id`, `tag_id`, `is_halt`, `create_datetime`, `modify_datetime`) VALUES('1','1','1','1','F',NULL,NULL);
INSERT INTO `article_tag` (`id`, `webapp_id`, `article_id`, `tag_id`, `is_halt`, `create_datetime`, `modify_datetime`) VALUES('2','1','2','2','F',NULL,NULL);
INSERT INTO `article_tag` (`id`, `webapp_id`, `article_id`, `tag_id`, `is_halt`, `create_datetime`, `modify_datetime`) VALUES('3','1','2','3','F',NULL,NULL);

DROP TABLE IF EXISTS `ly`;
CREATE TABLE `ly` (
  `id` bigint(16) NOT NULL AUTO_INCREMENT,
  `webapp_id` BIGINT(16) NOT NULL,
  `article_id` BIGINT(16),
  `ly_id` BIGINT(16),
  `user_id` BIGINT(16),
  `img` VARCHAR(256),
  `author` varchar(256) NOT NULL,
  `email` varchar(256),
  `url` VARCHAR(256),
  `content` TEXT,
  `ip` varchar(16),
  `ip_name` varchar(512),
  `is_halt` varchar(2) DEFAULT 'F',
  `create_datetime` datetime DEFAULT NULL,
  `modify_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `ly` (`id`, `webapp_id`, `article_id`, `ly_id`, `user_id`, `img`, `author`, `email`, `url`, `content`, `ip`, `ip_name`, `is_halt`, `create_datetime`, `modify_datetime`) VALUES('1','1','1',NULL,NULL,'75.jpg','旧城','http://jiucheng.org@gmail.com','http://blog.jiucheng.org','养了两年的罗汉松。','127.0.0.1',NULL,'F','2017-03-04 09:57:36','2017-03-04 09:57:36');
INSERT INTO `ly` (`id`, `webapp_id`, `article_id`, `ly_id`, `user_id`, `img`, `author`, `email`, `url`, `content`, `ip`, `ip_name`, `is_halt`, `create_datetime`, `modify_datetime`) VALUES('2','1','2',NULL,NULL,'26.jpg','知道91博客','zhidao91@yeah.net','http://www.zhidao91.com','博主博客是有什么写的啊，感觉像是WordPress呢','127.0.0.1',NULL,'F','2017-03-04 10:06:42','2017-03-04 10:06:42');

DROP TABLE IF EXISTS `link`;
CREATE TABLE `link` (
  `id` BIGINT(16) NOT NULL AUTO_INCREMENT,
  `webapp_id` BIGINT(16) NOT NULL,
  `uid` VARCHAR(64) NOT NULL,
  `name` VARCHAR(256) NOT NULL,
  `url` varchar(256) NOT NULL,
  `remark` TEXT,
  `is_halt` varchar(2) DEFAULT 'F',
  `fr` BIGINT(16) DEFAULT 0,
  `to` BIGINT(16) DEFAULT 0,
  `create_datetime` datetime DEFAULT NULL,
  `modify_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_0`(`webapp_id`,`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `link` (`id`, `webapp_id`, `uid`, `name`, `url`, `remark`, `is_halt`, `fr`, `to`, `create_datetime`, `modify_datetime`) VALUES('1','1','blogjiuchengorg','旧城博客','http://blog.jiucheng.org','一个神奇的博客','F','0','0','2017-03-04 09:50:05','2017-03-04 09:50:05');

DROP TABLE IF EXISTS `ip`;
CREATE TABLE `ip` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `start` VARCHAR(16) DEFAULT NULL,
  `end` VARCHAR(16) DEFAULT NULL,
  `name` VARCHAR(512) DEFAULT NULL,
  `start_num` BIGINT(16) DEFAULT NULL,
  `end_num` BIGINT(16) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_0` (`start_num`,`end_num`)
) ENGINE=INNODB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `log_uri`;
CREATE TABLE `log_uri` (
  `id` BIGINT(16) NOT NULL AUTO_INCREMENT,
  `webapp_id` BIGINT(16) NOT NULL,
  `uri` VARCHAR(255),
  `domain` VARCHAR(128),
  `ip` VARCHAR(16),
  `ip_name` varchar(256),
  `create_datetime` DATETIME,
  `modify_datetime` DATETIME,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `intercept_ip`;
CREATE TABLE `intercept_ip` (
  `id` BIGINT(16) NOT NULL AUTO_INCREMENT,
  `webapp_id` BIGINT(16) NOT NULL,
  `ip` VARCHAR(16) NOT NULL,
  `create_datetime` DATETIME DEFAULT NULL,
  `modify_datetime` DATETIME DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `index_0` (`webapp_id`,`ip`)
)ENGINE=INNODB DEFAULT CHARSET=utf8;

<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8" isELIgnored="false"%><%@ taglib prefix="c"
	uri="http://java.sun.com/jsp/jstl/core"%><%@ taglib prefix="fmt"
	uri="http://java.sun.com/jsp/jstl/fmt"%>
	<%@ taglib uri="http://jiucheng.org/jsp/taglib" prefix="tld"%>
<aside class="sidebar">
	<div class="widget widget_divCatalog">
		<h3 class="widget_tit">文章分类</h3>
		${tld:articleType('articleTypes')}
		<div>
		  <ul>
          <c:forEach var="articleType" items="${articleTypes}">
              <li class="li-cate cate-${articleType.id}"><a href="${website.url}/article/type/${articleType.id}">${articleType.name}<span class="article-nums">(${articleType.size})</span></a></li>
          </c:forEach>
          </ul>
		</div>
	</div>
	<div class="widget widget_divAboutKen">
		<h3 class="widget_tit">热门推荐</h3>
		${tld:rnd('rnd')}
		<div>
		  <c:forEach var="article" items="${rnd}">
		      <ul><li style="text-overflow:ellipsis;"><a href="${webapp.url}article/${article.id}" title="${article.title}">${article.title}</a></li></ul>
		  </c:forEach>
		</div>
	</div>
	<div class="widget widget_divFavorites">
		<h3 class="widget_tit">热门标签</h3>
		${tld:tagTop('tagTops')}
        <div>
        <ul>
        <c:forEach var="tag" items="${tagTops}">
            <li style="text-overflow:ellipsis;"><a href="${webapp.url}index?t=${tag.name}" title="${tag.name}">${tag.name}(${tag.size})</a></li>
        </c:forEach>
        </ul>
        </div>
	</div>
	<div class="widget widget_divComments">
        <h3 class="widget_tit">最新留言</h3>
        ${tld:lyNew('lyNews')}
        <div>
        <ul>
        <c:forEach var="ly" items="${lyNews}">
            <li style="text-overflow:ellipsis;"><a href="${webapp.url}article/${ly.articleId}#comments" title="${ly.content}">${ly.content}</a></li>
        </c:forEach>
        </ul>
        </div>
    </div>
	<div class="widget widget_divLinkage">
		<h3 class="widget_tit">友情链接</h3>
		${tld:linkTo('linkTos')}
		<div>
		<ul>
        <c:forEach var="link" items="${linkTos}">
            <li style="text-overflow:ellipsis;"><a href="${webapp.url}link/to/${link.uid}" title="${link.remark}" target="_blank">${link.name}</a></li>
        </c:forEach>
        </ul>
		</div>
	</div>
</aside>
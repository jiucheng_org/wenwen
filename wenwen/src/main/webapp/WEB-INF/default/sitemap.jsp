<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://jiucheng.org/jsp/taglib" prefix="jc"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>网站地图 - ${webapp.name} - ${webapp.descript} - ${webapp.domain}</title>
<style type="text/css">
.STYLE1 {
	font-size: 12px;
	color: #333333;
}
</style>
</head>
<body>
	<table align="center">
		<tr align="center">
			<td align="center">
				<table width="766" border="0">
                    <tr align="left">
                        <td class="STYLE1"><span>首页导航：</span></td>
                    </tr>
                    <tr align="left">
                        <td width="760" class="STYLE1">1.&nbsp;<a
                            href="${webapp.url }index">首页 - ${webapp.name} - ${webapp.descript} - ${webapp.domain}</a></td>
                    </tr>
                    <tr align="left">
                        <td width="760" class="STYLE1">2.&nbsp;<a
                            href="${webapp.url }article/type">分类 - ${webapp.name} - ${webapp.descript} - ${webapp.domain}</a></td>
                    </tr>
                    <tr align="left">
                        <td width="760" class="STYLE1">3.&nbsp;<a
                            href="${webapp.url }tag">标签 - ${webapp.name} - ${webapp.descript} - ${webapp.domain}</a></td>
                    </tr>
                    <tr align="left">
                        <td width="760" class="STYLE1">4.&nbsp;<a
                            href="${webapp.url }link">友链 - ${webapp.name} - ${webapp.descript} - ${webapp.domain}</a></td>
                    </tr>                    
					<tr align="left">
						<td class="STYLE1"><span>文章列表：</span></td>
					</tr>
					<c:forEach var="aritcle" items="${articles}" varStatus="status">
					<tr align="left">
						<td width="760" class="STYLE1">${status.count }.&nbsp;<a
							href="${webapp.url }article/${aritcle.id}">${aritcle.title }</a></td>
					</tr>
					</c:forEach>
				</table>
			</td>
		</tr>
	</table>
	<br />
</body>
</html>
"use strict";
+function(e) {
	e.ajaxSetup({
	    type: "post",
	    dataType: "json"
	});
	function n() {
		e(".butterBar").length > 0 && e(".butterBar").remove();
	}
	function c(t) {
		n(), l.append('<div class="butterBar butterBar--center"><p class="butterBar-message">' + t + "</p></div>"), setTimeout(function() {
			e(".butterBar").length > 0 && e(".butterBar").remove();
		}, 1000);
	}
	var l = jQuery("body");
	e("#commentform").on("submit", function(evt) {
		evt.preventDefault();
		var a = e(this);
		if(a.hasClass("is-active")) {
			c("别提交过快~");
			return false;
		}
		a.addClass("is-active");
	    e.ajax({
	        url: a.attr("action"),
	        type: a.attr("method"),
	        data: a.serialize() + "&tpl=underriver",
	        success: function (info) {
	        	a.removeClass("is-active");
	            if (info.code === 1) {
	                setTimeout(function () {
	                    location.href = info.url;
	                }, 1000);
	            }
	            c(info.msg);
	        },
	        error: function(fault) {
	        	a.removeClass("is-active");
	        	c("失败");
	        }
	    });
	    return false;
	});
}(jQuery);
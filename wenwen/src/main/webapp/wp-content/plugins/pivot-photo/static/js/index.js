jQuery(window).scroll(function() {
	var $pf = jQuery("#pf-show-more");
	if ($pf.length > 0 && $pf.data("paged")) {
		var q = jQuery(window).scrollTop(),
		p = $pf.offset().top,
		$this = $pf;
		if (q + jQuery(window).height() >= p && H != true) {
			var paged = $this.data("paged"),
			total = $this.data("total"),			
			post_type = $this.data("post_type");
			var ajax_data = {
				action: "pf_index_post",
				paged: paged,
				total: total,
				post_type: post_type
			};
			H = true;
			oconnorBody.addClass('is-loadingApp');
			jQuery.ajax({
				url: ajax.ajax_url,
				type: "POST",
				data: ajax_data,
				dataType: "json",
				success: function(data) {
					oconnorBody.removeClass('is-loadingApp');
					if (data.status == 200) {
						jQuery('#pf-show-more').remove();
						jQuery.each(data.data,
						function(i, item) {
							jQuery(".listGroup").append('<div class="photograph"><img class="img-responsive" src="' + item.thumb + '">' + item.like + '<a href="' + item.image + '!home" class="photo--link fancybox"><i class="iconfont icon-more"></i></a><div class="photo-message v-textAlignCenter">' + item.title + '</div></div>');
						});
						jQuery(".listGroup").after(data.nav);
						H = false;
					} else {
						jQuery('#pf-show-more').html('show more').removeClass('is-loading');
						console.log(data.status);
					}
				}
			});
			return false;
		}
	}
})
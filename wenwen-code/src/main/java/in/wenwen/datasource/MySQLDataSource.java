package in.wenwen.datasource;

import org.jiucheng.ioc.annotation.Component;
import org.jiucheng.ioc.annotation.Value;
import org.jiucheng.orm.PoolDataSource;

@Component("dataSource")
public class MySQLDataSource extends PoolDataSource {
    
    @Value("mysql.driverClass")
    private String driverClass;
    @Value("mysql.url")
    private String url;
    @Value("mysql.username")
    private String username;
    @Value("mysql.password")
    private String password;
    @Value("mysql.minActive")
    private Integer minActive;
    @Value("mysql.maxActive")
    private Integer maxActive;
    @Value("mysql.maxWait")
    private Long maxWait;

    @Override
    public String getDriverClass() {
        return driverClass;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public int getMinActive() {
        return minActive;
    }

    @Override
    public int getMaxActive() {
        return maxActive;
    }

    @Override
    public long getMaxWait() {
        return maxWait;
    }
}

package in.wenwen.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Date;

public class Wen {
    private static long ss = 1000;
    private static long mm = 60 * ss;
    private static long hh = 60 * mm;
    private static long dd = 24 * hh;
    private static long MM = 30 * dd;
    private static long yy = 12 * MM;
    public static String date(Date date) {
        if(date == null) {
            return "";
        }
        long today = Calendar.getInstance().getTimeInMillis();
        long time = date.getTime();
        long gap = today - time;
        if(gap  < mm) {
            return "刚刚";
        }
        if(gap < hh) {
            return (gap / mm) + "分钟前";
        }
        if(gap < dd) {
            return (gap / hh) + "小时前";
        }
        if(gap < MM) {
            return (gap / dd) + "天前";
        }
        if(gap < yy) {
            return (gap / MM) + "月前";
        }
        return (gap / yy) + "年前";
    }
    
    public static String num(Long num) {
        if(num != null) {
            if(num.longValue() < 512L) {
                return Long.toString(num.longValue());
            }
            double a = num.longValue() / 1024.00;
            BigDecimal b = new BigDecimal(Double.toString(a));
            return b.divide(BigDecimal.ONE, 2, RoundingMode.HALF_UP).toString() + "K";
        }
        return "";
    }
}

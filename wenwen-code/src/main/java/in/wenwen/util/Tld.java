package in.wenwen.util;

import in.wenwen.service.IArticleService;
import in.wenwen.service.IArticleTypeService;
import in.wenwen.service.ILinkService;
import in.wenwen.service.ILyService;
import in.wenwen.service.ITagService;

import java.util.Map;

import org.jiucheng.ioc.BeanFactory;
import org.jiucheng.util.StringUtil;
import org.jiucheng.web.util.WebUtil;

public class Tld {
    
    private static IArticleService articleService;
    private static IArticleTypeService articleTypeService;
    private static ITagService tagService;
    private static ILyService lyService;
    private static ILinkService linkService;
    
    public static ILinkService getLinkService() {
        if(linkService != null) {
            return linkService;
        }
        linkService = BeanFactory.get("linkService", ILinkService.class);
        return linkService;
    }
    
    public static ILyService getLyService() {
        if(lyService != null) {
            return lyService;
        }
        lyService = BeanFactory.get("lyService", ILyService.class);
        return lyService;
    }
    
    public static ITagService getTagService() {
        if(tagService != null) {
            return tagService;
        }
        tagService = BeanFactory.get("tagService", ITagService.class);
        return tagService;
    }
    
    public static IArticleService getArticleService() {
        if(articleService != null) {
            return articleService;
        }
        articleService = BeanFactory.get("articleService", IArticleService.class);
        return articleService;
    }
    
    public static IArticleTypeService getArticleTypeService() {
        if(articleTypeService != null) {
            return articleTypeService;
        }
        articleTypeService = BeanFactory.get("articleTypeService", IArticleTypeService.class);
        return articleTypeService;
    }
    
    public static void rnd(String id) {
        WebUtil.getRequest().setAttribute(id, getArticleService().getRnd(UserManage.getWebappId(), 12));
    }
    
    public static void rnd0(Map<String, Object> content, String id, Long limit) {
        if(limit == null || limit > 12L) {
            limit = 12L;
        }
        content.put(id, getArticleService().getRnd0(UserManage.getWebappId(), limit.intValue()));
    }
    
    public static void articleType(String id) {
        WebUtil.getRequest().setAttribute(id, getArticleTypeService().getBlogTypeDto(UserManage.getWebappId()));
    }
    
    public static void tagTop(String id) {
        WebUtil.getRequest().setAttribute(id, getTagService().getTopJson(UserManage.getWebappId(), 12L));
    }
    
    public static void tagTop0(Map<String, Object> content, String id, Long limit) {
        if(limit == null || limit > 18L) {
            limit = 18L;
        }
        content.put(id, getTagService().getTopJson(UserManage.getWebappId(), limit));
    }
    
    public static void lyNew(String id) {
        WebUtil.getRequest().setAttribute(id, getLyService().getNew(UserManage.getWebappId(), 12L));
    }
    
    public static void lyNew0(Map<String, Object> content, String id, Long limit) {
        if(limit == null || limit > 12L) {
            limit = 12L;
        }
        content.put(id, getLyService().getNew(UserManage.getWebappId(), limit));
    }
    
    public static void linkTo(String id) {
        WebUtil.getRequest().setAttribute(id, getLinkService().getNewFr(UserManage.getWebappId(), 12L));
    }
    
    public static String omit(String str, Long len) {
        if(str == null) {
            return StringUtil.EMPTY;
        }
        if(len != null && str.length() > len) {
            return str.substring(0, len.intValue()) + "...";
        }
        return str;
    }
    
    public static void ly(Map<String, Object> content, String id, Long articleId) {
        content.put(id, getLyService().getArticleLy(UserManage.getWebappId(), articleId, null));
    }
}

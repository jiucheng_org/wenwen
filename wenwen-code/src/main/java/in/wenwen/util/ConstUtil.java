package in.wenwen.util;

import org.jiucheng.util.DefaultPropertiesUtil;

public class ConstUtil {
    public static final String COMMA = ",";
    public static final String TOKEN = "token";
    public static final String ERR = "err";
    
    public static final String SMTP_HOST = "smtp.host";
    public static final String SMTP_PORT = "smtp.port";
    public static final String SMTP_USERNAME = "smtp.username";
    public static final String SMTP_PASSWORD = "smtp.password";
    public static final String SMTP_CHARSET = "smtp.charset";
    public static final String SMTP_FR = "smtp.fr";
    public static final String SMTP_NICK = "smtp.nick";
    
    private static String context;
    private static String contextEx;
    
    public static String getRedirectUri(String uri) {
        return getContext().concat(uri);
    }
    
    // / 或 /wenwen/
    public static String getContext() {
        if(context != null) {
            return context;
        }
        context = DefaultPropertiesUtil.getString("context", "/");
        return context;
    }
    
    // /wenwen 或者 空字符串
    public static String getContextEx() {
        if(contextEx != null) {
            return contextEx;
        }
        String context = getContext();
        contextEx = context.substring(0, context.length() - 1);
        return contextEx;
    }
    
    public static String getAdminUri(String jsp) {
        return "/WEB-INF/admin/".concat(jsp).concat(DefaultPropertiesUtil.getString("suffix", ".jsp"));
    }
    
    public static String getUri(String jsp) {
        return "/WEB-INF/".concat(UserManage.getTemplet()).concat("/").concat(jsp).concat(DefaultPropertiesUtil.getString("suffix", ".jsp"));
    }
}

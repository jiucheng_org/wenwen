package in.wenwen.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import in.wenwen.dto.ArticleDto;
import in.wenwen.dto.QueryDto;
import in.wenwen.entity.Article;
import in.wenwen.handler.AdminHandler;
import in.wenwen.out.UserOut;
import in.wenwen.service.IArticleService;
import in.wenwen.util.ConstUtil;
import in.wenwen.util.Err;
import in.wenwen.util.PageResult;
import in.wenwen.util.UserManage;

import org.jiucheng.exception.UncheckedException;
import org.jiucheng.ioc.annotation.Inject;
import org.jiucheng.util.StringUtil;
import org.jiucheng.web.annotation.Controller;
import org.jiucheng.web.annotation.Param;
import org.jiucheng.web.annotation.RequestMapping;
import org.jiucheng.web.annotation.RequestMethod;

@Controller(AdminHandler.class)
public class AdminArticleController {
    
    @Inject
    private IArticleService articleService;
    
    @RequestMapping(value = "/admin/article/index.html", method = RequestMethod.GET)
    public String index() {
        return ConstUtil.getAdminUri("article/index");
    }
    
    @RequestMapping(value = "/admin/article/page.json", method = RequestMethod.GET, out = UserOut.class)
    public PageResult<ArticleDto> json(@Param("") QueryDto queryDto) {
        queryDto.setWebappId(UserManage.getWebappId());
        PageResult<ArticleDto> pru = articleService.pageDto(queryDto);
        return pru;
    }
    
    @RequestMapping(value = "/admin/article/add.html", method = RequestMethod.GET)
    public String add() {
        return ConstUtil.getAdminUri("article/add");
    }
    
    @RequestMapping(value = "/admin/article/add.html", method = RequestMethod.POST)
    public String addPost(@Param("article") Article article, HttpServletRequest request) {
        article.setId(null);
        article.setWebappId(UserManage.getWebappId());
        article.setUserId(UserManage.getUserDto().getId());
        article.setModifyDatetime(UserManage.getReceiveDatetime());
        article.setCreateDatetime(UserManage.getReceiveDatetime());
        articleService.saveOrUpdateArticle(article);
        request.setAttribute("article", article);
        articleService.clearCache();
        return ConstUtil.getAdminUri("article/add-success");
    }
    
    @RequestMapping(value = "/admin/article/delete.html", method = RequestMethod.GET, out = UserOut.class)
    public Err delete(HttpServletRequest request) {
        Err err = new Err("删除失败");
        List<Long> ids = new ArrayList<Long>();
        String id;
        for(int i = 0; i < 100; i ++) {
            id = request.getParameter("id[" + i + "]");
            if(StringUtil.isBlank(id)) {
                break;
            }
            if(id.matches("[0-9]+")) {
                ids.add(Long.parseLong(id));
            }else {
                ids = new ArrayList<Long>();
                break;
            }
        }
        if(ids.size() == 0) {
            return err;
        }
        articleService.deleteUserByIds(UserManage.getWebappId(), ids);
        err.setErr("0");
        articleService.clearCache();
        return err;
    }
    
    @RequestMapping(value = "/admin/article/edit.html", method = RequestMethod.GET)
    public String edit(HttpServletRequest request) {
        String idStr = request.getParameter("id");
        if(StringUtil.isNotBlank(idStr) && idStr.matches("[0-9]+")) {
            Long id = Long.parseLong(idStr);
            Article article = new Article();
            article.setId(id);
            List<Article> useres = articleService.list(article);
            if(useres.size() == 0 || !UserManage.getWebappId().equals(useres.get(0).getWebappId())) {
                throw new UncheckedException("非法操作");
            }
            request.setAttribute("article", useres.get(0));
        }
        return ConstUtil.getAdminUri("article/edit");
    }
    
    @RequestMapping(value = "/admin/article/edit.html", method = RequestMethod.POST)
    public String editPost(@Param("article") Article article, HttpServletRequest request) {
        if(article.getId() != null) {
            article.setModifyDatetime(UserManage.getReceiveDatetime());
            articleService.saveOrUpdateArticle(article);
            request.setAttribute("article", article);
            articleService.clearCache();
        }
        return ConstUtil.getAdminUri("article/edit-success");
    }
    
    @RequestMapping(value = "/admin/article/detail.html", method = RequestMethod.GET)
    public void detail() {
        
    }
}

package in.wenwen.controller;

import org.jiucheng.web.annotation.Controller;
import org.jiucheng.web.annotation.RequestMapping;
import org.jiucheng.web.annotation.RequestMethod;

import in.wenwen.handler.IndexHandler;
import in.wenwen.out.TplOut;
import in.wenwen.util.ConstUtil;

@Controller(IndexHandler.class)
public class AboutController {
    
    @RequestMapping(value = "/about", method = RequestMethod.GET, out = TplOut.class)
    public String about() {
        return ConstUtil.getUri("about");
    }
}

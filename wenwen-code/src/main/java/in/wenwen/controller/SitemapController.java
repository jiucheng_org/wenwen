package in.wenwen.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import in.wenwen.entity.Article;
import in.wenwen.handler.IndexHandler;
import in.wenwen.service.IArticleService;
import in.wenwen.util.ConstUtil;
import in.wenwen.util.UserManage;

import org.jiucheng.ioc.annotation.Inject;
import org.jiucheng.web.annotation.Controller;
import org.jiucheng.web.annotation.RequestMapping;

@Controller(IndexHandler.class)
public class SitemapController {
    
    @Inject
    private IArticleService articleService;
    
    @RequestMapping("/sitemap.html")
    public String sitemap(HttpServletRequest request) {
        List<Article> articles = articleService.listArticle(UserManage.getWebappId());
        if(null == articles) {
            articles = new ArrayList<Article>();
        }
        request.setAttribute("articles", articles);
        return ConstUtil.getUri("sitemap");
    }
}

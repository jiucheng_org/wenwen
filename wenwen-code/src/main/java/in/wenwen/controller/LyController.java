package in.wenwen.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import in.wenwen.dto.InfoDto;
import in.wenwen.dto.LyDto;
import in.wenwen.entity.Article;
import in.wenwen.entity.Ly;
import in.wenwen.handler.IndexHandler;
import in.wenwen.out.JSONStringOut;
import in.wenwen.service.IArticleService;
import in.wenwen.service.ILyService;
import in.wenwen.util.ConstUtil;
import in.wenwen.util.HtmlUtil;
import in.wenwen.util.UserManage;

import org.jiucheng.ioc.annotation.Impl;
import org.jiucheng.ioc.annotation.Inject;
import org.jiucheng.util.StringUtil;
import org.jiucheng.web.annotation.Controller;
import org.jiucheng.web.annotation.Param;
import org.jiucheng.web.annotation.RequestMapping;
import org.jiucheng.web.annotation.RequestMethod;
import org.jiucheng.web.handler.Out;
import org.jiucheng.web.util.WebUtil;

@Controller(IndexHandler.class)
public class LyController {
    
    @Inject
    private ILyService lyService;
    @Inject
    private IArticleService articleService;
    @Impl(JSONStringOut.class)
    private Out out;
    
    @RequestMapping(value = {"/ly/new/json", "/ly/new.json"}, method=  RequestMethod.GET, out = JSONStringOut.class)
    public List<Ly> newJson() {
        return lyService.getNew(UserManage.getWebappId(), 12L);
    }
    
    @RequestMapping(value="/article/([0-9]+)/ly", method=RequestMethod.GET, out = JSONStringOut.class)
    public List<LyDto> articleLy(String[] args){
        return lyService.getArticleLy(UserManage.getWebappId(), Long.parseLong(args[1]), null);
    }
    
    @RequestMapping(value = "/article/ly", method = RequestMethod.POST)
    public void articleLy(String[] args, @Param("ly") Ly ly,
            @Param("kaptcha") String kaptcha,
            HttpServletRequest req, HttpServletResponse response,
            HttpSession session) {
        boolean isUnderriver = false;
        if("underriver".equals(UserManage.getWebappDto().getTemplet())) {
            isUnderriver = true;
        }
        if(null == ly.getArticleId()) {
            WebUtil.redirect(ConstUtil.getRedirectUri("index").concat("?fr=").concat(args[0]));
            return;
        }
        if(StringUtil.isBlank(ly.getContent()) || StringUtil.isBlank(ly.getAuthor())) {
            WebUtil.redirect(ConstUtil.getRedirectUri("article/") + ly.getArticleId());
            return;
        }
        System.out.println("isUnderriver=" + isUnderriver);
        // 验证验证码
        if(isUnderriver) {
            String cha = (String) session.getAttribute(com.google.code.kaptcha.Constants.KAPTCHA_SESSION_KEY);
            System.out.println("kaptcha=" + cha + ", from=" + kaptcha);
            if(StringUtil.isBlank(kaptcha) || StringUtil.isBlank(cha) || !kaptcha.equalsIgnoreCase(cha)) {
                out.invoke(null, InfoDto.newInstance(-1, "验证码错误", ""));
                return;
            }
            String tpl = req.getParameter("tpl");
            if(!"underriver".equals(tpl)) {
                out.invoke(null, InfoDto.newInstance(-1, "验证码错误~", ""));
                return;
            }
        }
        Article article = lyService.get(Article.class, ly.getArticleId());
        if(null == article || !UserManage.getWebappId().equals(article.getWebappId())) {
            WebUtil.redirect(ConstUtil.getRedirectUri("index").concat("?fr=").concat(args[0]));
            return;
        }
        ly.setContent(HtmlUtil.html(ly.getContent()));
        ly.setAuthor(HtmlUtil.html(ly.getAuthor()));
        ly.setUrl(HtmlUtil.html(ly.getUrl()));
        ly.setEmail(HtmlUtil.html(ly.getEmail()));
        lyService.saveLyUpdateArticle(ly, article);
        articleService.clearCache();
        if(isUnderriver) {
            out.invoke(null, InfoDto.newInstance(1, "成功", ConstUtil.getRedirectUri("article/") + ly.getArticleId()));
        }else {
            WebUtil.redirect(ConstUtil.getRedirectUri("article/") + ly.getArticleId());
        }
    }
}

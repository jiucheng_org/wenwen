package in.wenwen.controller;

import javax.servlet.http.HttpServletRequest;

import in.wenwen.entity.Webapp;
import in.wenwen.handler.AdminHandler;
import in.wenwen.service.IWebappService;
import in.wenwen.util.ConstUtil;
import in.wenwen.util.UserManage;

import org.jiucheng.ioc.annotation.Inject;
import org.jiucheng.util.StringUtil;
import org.jiucheng.web.annotation.Controller;
import org.jiucheng.web.annotation.Param;
import org.jiucheng.web.annotation.RequestMapping;
import org.jiucheng.web.annotation.RequestMethod;

@Controller(AdminHandler.class)
public class AdminWebappController {

    @Inject
    private IWebappService webappService;

    @RequestMapping(value = "/admin/webapp/edit.html", method = RequestMethod.GET)
    public String edit(HttpServletRequest request) {
        Webapp zhan = webappService.get(Webapp.class, UserManage.getWebappId());
        request.setAttribute("zhan", zhan);
        return ConstUtil.getAdminUri("webapp/edit");
    }

    @RequestMapping(value = "/admin/webapp/edit.html", method = RequestMethod.POST)
    public String editPost(@Param("name") String name,
            @Param("descript") String descript, HttpServletRequest request) {
        if (StringUtil.isNotBlank(name) && StringUtil.isNotBlank(descript)) {
            Webapp webapp = new Webapp();
            webapp.setId(UserManage.getWebappId());
            webapp.setName(name);
            webapp.setDescript(descript);
            webapp.setModifyDatetime(UserManage.getReceiveDatetime());
            webappService.update(webapp);
        }
        return ConstUtil.getAdminUri("webapp/edit-success");
    }
}

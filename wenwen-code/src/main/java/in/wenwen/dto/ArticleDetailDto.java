package in.wenwen.dto;

import in.wenwen.entity.Article;

public class ArticleDetailDto {
    private ArticleDto articleDto;
    private Article prev;
    private Article next;

    public ArticleDto getArticleDto() {
        return articleDto;
    }

    public void setArticleDto(ArticleDto articleDto) {
        this.articleDto = articleDto;
    }

    public Article getPrev() {
        return prev;
    }

    public void setPrev(Article prev) {
        this.prev = prev;
    }

    public Article getNext() {
        return next;
    }

    public void setNext(Article next) {
        this.next = next;
    }
}

package in.wenwen.service;

import org.jiucheng.plugin.db.IBaseService;

public interface IInterceptIpService extends IBaseService {
    /**
     * 是否是拦截的IP
     * @param webappId
     * @param ip
     * @return
     */
    public boolean isIntercepted(Long webappId, String ip);
}

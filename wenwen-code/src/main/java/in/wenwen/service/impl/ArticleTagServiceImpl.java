package in.wenwen.service.impl;

import in.wenwen.service.IArticleTagService;

import org.jiucheng.aop.Aop;
import org.jiucheng.ioc.annotation.Service;
import org.jiucheng.orm.interceptor.Close;
import in.wenwen.service.impl.BaseServiceImpl;

@Service("articleTagService")
@Aop(Close.class)
public class ArticleTagServiceImpl extends BaseServiceImpl implements IArticleTagService {

}

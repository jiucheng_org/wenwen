package in.wenwen.service.impl;

import org.jiucheng.ioc.annotation.Inject;
import org.jiucheng.plugin.db.IBaseDao;

public class BaseServiceImpl extends org.jiucheng.plugin.db.BaseServiceImpl {
    
    @Inject
    private IBaseDao baseDao;

    @Override
    public IBaseDao getBaseDao() {
        return baseDao;
    }

}

package in.wenwen.service.impl;

import java.util.List;

import in.wenwen.dao.IBaseDao;
import in.wenwen.entity.InterceptIp;
import in.wenwen.service.IInterceptIpService;

import org.jiucheng.aop.Aop;
import org.jiucheng.ioc.annotation.Inject;
import org.jiucheng.ioc.annotation.Service;
import org.jiucheng.orm.interceptor.Close;
import org.jiucheng.util.StringUtil;

@Service("interceptIpService")
@Aop(Close.class)
public class InterceptIpServiceImpl extends BaseServiceImpl implements IInterceptIpService {
    
    @Inject
    private IBaseDao baseDao;
    
    public boolean isIntercepted(Long webappId, String ip) {
        if(StringUtil.isBlank(ip)) {
            return true;
        }
        InterceptIp interceptIp = new InterceptIp();
        interceptIp.setWebappId(webappId);
        interceptIp.setIp(ip);
        List<InterceptIp> list = baseDao.list(interceptIp);
        if(null != list && list.size() > 0) {
            return true;
        }
        return false;
    }
}

package in.wenwen.out;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jiucheng.template.Template;
import org.jiucheng.web.WebWrapper;
import org.jiucheng.web.handler.Out;
import org.jiucheng.web.util.WebUtil;

import in.wenwen.util.Tld;
import in.wenwen.util.Wen;

public class TplOut implements Out {
    
    public TplOut() {
        Template.putClass("tld", Tld.class);
        Template.putClass("wen", Wen.class);
    }

    public void invoke(WebWrapper webWrapper, Object rs) {
        if(null == rs) {
            return;
        }
        HttpServletResponse response = WebUtil.getResponse();
        response.setCharacterEncoding(WebUtil.getEncoding());
        response.setContentType("text/html;charset=" + WebUtil.getEncoding());
        if(rs instanceof String) {
            String filePath = (String) rs;
            if(filePath.indexOf("underriver") != -1) {
                HttpServletRequest req = WebUtil.getRequest();
                Enumeration<String> enums = req.getAttributeNames();
                Map<String, Object> context = new HashMap<String, Object>();
                if(enums != null) {
                    String k;
                    while(enums.hasMoreElements()) {
                        k = enums.nextElement();
                        context.put(k, req.getAttribute(k));
                    }
                }
                filePath = WebUtil.getWebRoot() + filePath.substring(1, filePath.lastIndexOf(".") + 1) + "html";
                try {
                    PrintWriter out = response.getWriter();
                    out.print(Template.get(context, filePath));
                    out.flush();
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }else {
                WebUtil.dispatcher(rs.toString());
            }
        }else {
            try {
                PrintWriter out = response.getWriter();
                out.print(rs);
                out.flush();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

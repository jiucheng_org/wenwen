package in.wenwen.entity;

public class InterceptIp extends BaseEntity {

    /**
     * 
     */
    private static final long serialVersionUID = -7171069308805181385L;

    private String ip;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }
}

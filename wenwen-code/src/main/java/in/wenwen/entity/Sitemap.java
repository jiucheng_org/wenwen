package in.wenwen.entity;

import java.io.Serializable;

public class Sitemap implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6836428994270477028L;

    private String uri;
    private String name;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}

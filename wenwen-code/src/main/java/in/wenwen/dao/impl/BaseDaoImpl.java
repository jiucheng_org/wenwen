package in.wenwen.dao.impl;

import javax.sql.DataSource;

import org.jiucheng.ioc.annotation.Impl;
import org.jiucheng.ioc.annotation.Inject;
import org.jiucheng.ioc.annotation.Repository;
import org.jiucheng.orm.dialect.Dialect;
import org.jiucheng.orm.dialect.impl.MySQLDialect;

import in.wenwen.dao.IBaseDao;

@Repository("baseDao")
public class BaseDaoImpl extends org.jiucheng.plugin.db.BaseDaoImpl implements IBaseDao {
    
    @Impl(MySQLDialect.class)
    private Dialect dialect;
    
//    @Impl(DruidDataSourceBuilder.class)
    @Inject
    private DataSource dataSource;
    
    @Override
    public Dialect getDialect() {
        return dialect;
    }

    @Override
    public DataSource getDataSource() {
        return dataSource;
    }
}
